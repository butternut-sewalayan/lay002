@extends('master',['bodyclass'=>'menuleft-open'])

@push('prestyle')
@endpush

@push('styles')
@endpush

@section('contentbody')
    <div class="wrapper">
        @include('includes.header')
        @include('includes.sidebar')
	    <div class="main-container">
            <div class="container-fluid mb-3 position-relative bg-redish">
                <div class="row">
                    <div class="container py-2">
                        <div class="row page-title-row">
                            <div class="col-8 col-md-6">
                                <h2 class="page-title text-white">Blank</h2>
                                <p class="text-white">Bootstrap Admin Dashboard Template with wide range components</p>
                            </div>
                            
                        </div>

                    </div>
                </div>
            </div>

            <!-- Begin page content -->
            <div class="container">
                <div class="row">

                </div>
            </div>
        </div>
        @include('includes.sidebar_right')
        @include('includes.footer')
    </div>
@endsection

@push('scripts')
@endpush

@push('scripts2')
@endpush