@extends('master')

@push('prestyle')
@endpush

@push('styles')
	<link id="theme" rel="stylesheet" href="{{asset('css/logon.css')}}" type="text/css">
@endpush

@section('contentbody')
    <div class="wrapper pt-0">
        <div class="main-container ">
			<div class="container-fluid p-0">
				<div class="row no-gutters vh-100 lgn">
					<div class="col-sm-12 col-md-6 col-lg-5 bg-white">
						<div class="card rounded-0 bg-none border-0 mb-3 signin-block2">
							@if ($errors->any())
								<div class="alert alert-danger mg-b-0" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close">
										<span aria-hidden="true">×</span>
									</button>
									<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
									</ul>
								</div>
							@endif
							<div class="card-body pr-5 pl-5">
								{!! BootForm::open(['url' => route('login'), 'method' => 'post']) !!}
								<h1 class="display-4 text-center d-block">Login</h1>
								<br>
								<div class="form-group text-left float-label">
									<input name="email" type="email" class="form-control" value="{{ old('email') }}" autocomplete="off" required>
									<label>Email</label>
								</div>
								<div class="form-group text-left float-label input-group" id="showhidepass">
									<input name="password" type="password" class="form-control" required autocomplete="off">
									<label>Password</label>
									<div class="input-group-addon">
										<a href="#" class="lgn-eye"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
									</div>
								</div>
								<div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input" id="customCheck1" {{ old('remember') ? 'checked' : '' }}>
									<label class="custom-control-label" for="customCheck1"> Ingat Saya</label>
								</div>
								<br>
								<div>
									<button type="submit" class="btn btn-primary btn-rounded btn-block col z2">Login</button>
									<br>
								</div>
								{!! BootForm::close() !!}
							</div>
						</div>
					</div>
					<div class="col-sm-12 col-md-6 col-lg-7 bg-dark">
						<div class="background">
							<img src="{{asset('img/bg-modern.png')}}" alt="background-pasar" class="full-opacity">
						</div>
						<div class="p-5 mt-5 text-white text-center">
							<h1 class="display-1 login-largetitle"></h1>
							<h2 class="f-light text-uppercase"></h2>
							<br>
							<hr>
							<br>
							<p class="lead"></p>
							<br>

						</div>
					</div>
				</div>
			</div>
        </div>
        @include('includes.footer',['class'=>'bg-dark'])
    </div>
@endsection

@push('scripts')
@endpush

@push('scripts2')
@endpush